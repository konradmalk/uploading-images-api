from rest_framework import serializers

from image.models import ImageCollection, Image


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'name', 'url')


class ImageCollectionSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = ImageCollection
        fields = ('counter', 'images')
