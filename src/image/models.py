from django.db import models

from user.models import User


class ImageCollection(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    counter = models.PositiveIntegerField(null=True, blank=True)

    def __str__(self):
        return self.description


class Image(models.Model):
    image_collection = models.ForeignKey(
        ImageCollection,
        related_name='images',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    name = models.TextField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    bucket_path = models.TextField(null=True, blank=True)
