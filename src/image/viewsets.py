from django.conf import settings
from rest_framework import status, viewsets
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_500_INTERNAL_SERVER_ERROR
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.response import Response

from google.cloud import storage

from image.models import ImageCollection, Image
from image.serializers import ImageCollectionSerializer
from image import utils


def get_storage_client():
    # This is for the purpose of mocking in unit tests.
    return storage.Client()


class ImageViewSet(viewsets.ModelViewSet):
    queryset = ImageCollection.objects.all()
    serializer_class = ImageCollectionSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JWTAuthentication,)

    def get_queryset(self):
        user = self.request.user
        return ImageCollection.objects.filter(user=user).prefetch_related('images')

    def create(self, request, *args, **kwargs):
        user = self.request.user
        image_file = request.data.get('image')
        try:
            # Check if the file in request is a valid image.
            if not utils.is_valid_image(image_file):
                return Response({'error': 'Wrong format. Provide .jpg or .png file.'}, status=HTTP_400_BAD_REQUEST)

            # Create ImageCollection object.
            image_collection_object = ImageCollection.objects.create(
                user=user,
                counter=utils.get_next_file_number(user)
            )
            utils.upon_collection_creation(
                collection=image_collection_object,
                file=image_file,
                storage_client=get_storage_client()
            )

            return Response(
                f"Image {image_collection_object.counter} uploaded successfuly.",
                status=status.HTTP_201_CREATED
            )
        except Exception as e:
            return Response({'error': str(e)}, status=HTTP_500_INTERNAL_SERVER_ERROR)


class ExpiringLinkViewSet(viewsets.ViewSet):
    def create(self, request):
        image_id = request.data.get('id')
        expiration_time = request.data.get('time')

        # First, validate and get the Image object.
        try:
            image = Image.objects.get(id=image_id)
        except Image.DoesNotExist:
            return Response({'error': 'Image not found.'}, status=HTTP_404_NOT_FOUND)
        except ValueError:
            return Response({'error': 'Provide correct numerical id.'}, status=HTTP_400_BAD_REQUEST)

        # Then, validate and get the expiration time provided in request.
        try:
            expiration_time = int(expiration_time)
        except ValueError:
            return Response({'error': 'Provide a number for expiration time.'}, status=HTTP_400_BAD_REQUEST)
        except TypeError:
            return Response({'error': 'Provide a number for expiration time.'}, status=HTTP_400_BAD_REQUEST)

        if expiration_time < 300 or expiration_time > 30000:
            return Response(
                {'error': 'Provide expiration time between 300 and 30000 seconds.'},
                status=HTTP_400_BAD_REQUEST
            )

        # Connect to Google Cloud bucket.
        bucket_name = settings.GS_BUCKET_NAME
        storage_client = get_storage_client()
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(image.bucket_path)

        # Generate signed url with given expiration time
        signed_url = blob.generate_signed_url(expiration=expiration_time, version='v4')

        return Response({'expiring_link': signed_url}, status=200)
