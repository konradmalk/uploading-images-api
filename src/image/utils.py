import io

from django.core.exceptions import BadRequest
from django.conf import settings

from PIL import Image as PilImage

from user.models import AllowedHeight
from image.models import ImageCollection, Image


def is_valid_image(file):
    # Validate if file sent by the user is an image and is in JPEG or PNG format.
    try:
        img = PilImage.open(file)
        if img.format in ('JPEG', 'PNG'):
            img.verify()
            return True
        else:
            return False
    except Exception:
        return False


def get_next_file_number(username):
    # Get a number which will be assigned to next image collection created after upload.
    latest_file = ImageCollection.objects.filter(user__username=username).order_by('-id').first()
    if latest_file:
        return latest_file.counter + 1
    else:
        return 1


def create_image_db_object(collection, name, target_path):
    # Create a db object for the image.
    image_object = Image.objects.create(
        image_collection=collection,
        name=name,
        url=f'https://storage.googleapis.com/images_uploading/{target_path}',
        bucket_path=target_path,
    )
    return image_object


def process_and_upload_image(collection, file, storage_client, height=0, original=False):
    try:
        # Get extension from the uploaded file.
        file_extension = file.name.split('.')[-1]

        # Resize the image in accordance with given param.
        img = PilImage.open(file)
        if original:
            max_height = img.height
        else:
            max_height = height
        img.thumbnail((img.width, max_height))

        # Decide on the file name.
        if original:
            name = "original"
        else:
            name = f'{img.width}x{max_height}'

        # Create the target path with the username, file number, and original file extension.
        target_path = f'{collection.user.username}/{collection.counter}/{name}.{file_extension}'

        # Save the resized image.
        resized_image_data = io.BytesIO()
        img.save(resized_image_data, format=file_extension.upper())
        resized_image_data.seek(0)

        # Connect to Google Cloud bucket.
        bucket_name = settings.GS_BUCKET_NAME
        bucket = storage_client.get_bucket(bucket_name)
        blob_name = target_path
        blob = bucket.blob(blob_name)

        # Upload the resized image data to bucket.
        blob.upload_from_file(resized_image_data, content_type=f'image/{file_extension.lower()}')

        # Create the db object corresponding with the uploaded image.
        image_object = create_image_db_object(collection=collection, name=name, target_path=target_path)

        return image_object

    except Exception:
        raise BadRequest()


def upon_collection_creation(collection, file, storage_client):
    # If the user has sufficient account tier, handle the original upload first.
    if collection.user.account_tier.link_to_original:
        process_and_upload_image(collection=collection, file=file, original=True, storage_client=storage_client)

    # Then, get the list of desirable heights from user's account tier.
    allowed_heights = AllowedHeight.objects.filter(tier=collection.user.account_tier)
    heights_list = list(allowed_heights.values_list('height', flat=True))

    # And iterate through each height, creating a corresponding resource in bucket.
    for each in heights_list:
        process_and_upload_image(collection=collection, file=file, height=each, storage_client=storage_client)
