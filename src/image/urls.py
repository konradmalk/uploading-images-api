from django.urls import path, include
from rest_framework.routers import DefaultRouter
from image.viewsets import ImageViewSet, ExpiringLinkViewSet

router = DefaultRouter()
router.register(r'expiring_link', ExpiringLinkViewSet, basename='expiring_link')
router.register(r'', ImageViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
