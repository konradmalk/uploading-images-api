# The task
Using Django REST Framework, write an API that allows any user to upload an image in PNG or JPG format.

You are allowed to use any libraries or base projects / cookie cutters you want (but using DRF is a hard requirement).

Skip the registration part, assume users are created via the admin panel.

## Requirements:
- it should be possible to easily run the project. docker-compose is a plus
- users should be able to upload images via HTTP request
- users should be able to list their images
- there are three builtin account tiers: Basic, Premium and Enterprise:
- users that have "Basic" plan after uploading an image get: 
  - a link to a thumbnail that's 200px in height
- users that have "Premium" plan get:
  - a link to a thumbnail that's 200px in height
  - a link to a thumbnail that's 400px in height
  - a link to the originally uploaded image
- users that have "Enterprise" plan get
  - a link to a thumbnail that's 200px in height
  - a link to a thumbnail that's 400px in height
  - a link to the originally uploaded image
  - ability to fetch an expiring link to the image (the link expires after a given number of seconds (the user can specify any number between 300 and 30000))
- apart from the builtin tiers, admins should be able to create arbitrary tiers with the following things configurable:
  - arbitrary thumbnail sizes
  - presence of the link to the originally uploaded file
  - ability to generate expiring links
- admin UI should be done via django-admin
- there should be no custom user UI (just browsable API from Django Rest Framework)
- remember about:
  - tests
  - validation
  - performance considerations (assume there can be a lot of images and the API is frequently accessed)


Please focus on code cleanliness and quality.

# Execution
## Installation
1. Requirements
   1. Use `python 3.11` 
   2. Install pip-tools `pip install pip-tools`
2. Django
   1. Copy `.env.example` as `.env`
   2. Edit `.env' and provide own values for variables:
      1. `GOOGLE_STORAGE_BUCKET_NAME`
      2. `GS_BUCKET_NAME`
      3. `GOOGLE_APPLICATION_CREDENTIALS_FILE`
   3. Install requirements `pip-sync`
   4. Run migrations `./manage.py migrate`
   5. Load fixtures `./manage.py ./user/fixtures/data.json`
   6. Create Superuser, if needed `./manage.py createsuperuser`
   7. Use provided docker file `docker compose up -d`

## Models
1. **user.models.User**
   - uses default django AbstractUser,
   - has relation of User to AccountTier, so we can tell which user has which plan.
2. **user.models.AccountTier**
   - used to define scope of functionalities allowed for particular user,
3. **user.models.AllowedHeight**
   - has relation to AccountTier, so we can tell what heights are allowed for the particular plan,
   - Using AllowedHeight model in relation to AccountTier may not be the most efficient solution to the requirement of being able to define custom allowed image heights for account tier at administrators' discretion. This way was chosen for its simplicity.
4. **image.models.ImageCollection**
   - is created when user uploads an image file,
   - serves as an anchor for Image or Images created after particular upload,
   - has counter which serves to differentiate results of various uploads done by the user,
   - has methods to handle the uploads to Google Bucket and creation of corresponding Image objects.
5. **image.models.Image**
   - contains information about corresponding Bucket resource and user-readable url to resource, 
   - has relation to ImageCollection, so we know which upload was the source of this particular Image,
   - this relation allows for simple presentation of data in response to the user by using nested serializer.
   - 
## Endpoints
### 1. Get JWT token
#### `POST /api/token/`

**Request:**
- **Body**: Form data with a field named `username` containing user's username and field `password` containing user's password.

**Response:**
- `200 OK`: Tokens generated successfuly.
- `401 Unauthorized`: Missing or invalid credentials.

### 2. Upload image
#### `POST /api/image/upload/`

**Request:**
- **Body**: Form data with a field named `image` containing the image file.

**Response:**
- `201 Created`: Image uploaded successfully.
- `400 Bad Request`: Invalid image file.
- `401 Unauthorized`: Missing or invalid JWT token.

### 3. Get all images
#### `GET /api/image/`

**Response:**
- `200 OK`: List of all image collections and images' thumbnails within them.
- `401 Unauthorized`: Missing or invalid JWT token.


### 4. Get expiring link
#### `POST /api/image/expiring_link/`

**Request:**
- **Body**: Form data with a field named `id` containing the id of a desired image and a field named `time` containing numerical value between 300 and 30000.

**Response:**
- `200 OK`: Expiring link created successfuly.
- `400 Bad Request`: Invalid values in request body or image with given ID not found.
- `401 Unauthorized`: Missing or invalid JWT token.

## Ending notes
- The links generated for the images are public and the directory naming scheme makes it easy to access other user's images. Handling authorization to view the resources in the bucket was considered out of scope for this task.
- User gets "clean"-looking URLs for their regular thumbnails (ex. https://storage.googleapis.com/images_uploading/user/1/247x200.png) and much, much longer and "unclean"-looking expiring URLs. This was done on purpose given the different character of purposes the user may have for requesting the links. Another reason is increased complexity with handling that with Google Cloud Buckets.