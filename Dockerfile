FROM python:3.11

# Python image settings
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

# Set workdir
WORKDIR /app

# We copy requirements to different location to avoid rebuilding
COPY src/requirements.txt /requirements/requirements.txt
RUN pip install -r /requirements/requirements.txt

# We copy our project to /app directory
COPY src /app/

# Expose Default port
EXPOSE 8000

# Set default command
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
